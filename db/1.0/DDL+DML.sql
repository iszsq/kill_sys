--  秒杀系统数据库脚本
drop database if exists db_kill;

create database if not exists db_kill
character set utf8
collate utf8_general_ci;

use db_kill;

-- ################商家表
-- 创建表
drop table if exists t_business;
create table t_business(
	id int unsigned auto_increment primary key,
	uuid varchar(32) not null comment 'uuid',
	bs_name varchar(30) not null comment '商家名称'
) engine=innodb
comment '商家表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;

-- 一般会有按商家名字搜索的需求 , 给名字加索引
alter table t_business add index idx_bs_name(bs_name);
-- uuid index
alter table t_business add index idx_uuid(uuid);

-- 初始化数据
insert into t_business(uuid, bs_name) values
('03474271fc7e11ea9fd01831bfdec6d6', '美团'),
('200ea921fc7e11ea9fd01831bfdec6d6', '饿了么'),
('23b51be1fc7e11ea9fd01831bfdec6d6', '淘宝');
-- ################商家表 end

-- ################商品表
-- 创建表
drop table if exists t_goods;
create table t_goods(
	id int unsigned auto_increment primary key,
	uuid varchar(32) not null comment 'uuid',
	bs_uuid varchar(32) default null comment '商家uuid',
	title varchar(30) not null comment '标题',
	price decimal(18,2) default 0 comment '售价',
	pic varchar(200) default '' comment '商品图片',
	stock int(10) default 0 comment '库存'
) engine=innodb
comment '商品表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;

alter table t_goods add index idx_uuid(uuid);
-- 标题索引
alter table t_goods add index idx_title(title);


-- 初始化数据
insert into t_goods(uuid, bs_uuid, title, price, pic) values
('c7ab5511fc7f11ea9fd01831bfdec6d6', '03474271fc7e11ea9fd01831bfdec6d6', '三星S20+  12GB+128GB 遐想灰', 7999.00, 'https://img13.360buyimg.com/seckillcms/s250x250_jfs/t1/99216/24/13256/301639/5e54fc16Ebdf3856e/f97e382b96d94f3e.jpg'),

('a0939771fc8011ea9fd01831bfdec6d6', '200ea921fc7e11ea9fd01831bfdec6d6', '3M KN90防尘防颗粒物防护口罩', 138.00, 'https://img14.360buyimg.com/seckillcms/s250x250_jfs/t1/146195/34/4191/83982/5f23b475E66cabb75/3eb70f57bed1b9bc.jpg'),

('a4819a81fc8011ea9fd01831bfdec6d6', '23b51be1fc7e11ea9fd01831bfdec6d6', '实木换鞋凳鞋柜', 459.00, 'https://img10.360buyimg.com/seckillcms/s250x250_jfs/t1/137788/20/1929/164022/5efe97e6E411c88c6/c9af900d35d17ccd.jpg');
-- ################商品表 end


-- ################用户表
-- 创建表
drop table if exists t_user;
create table t_user(
	id int unsigned auto_increment primary key,
	uuid varchar(32) not null comment 'uuid',
	username varchar(20) not null comment '用户名'
) engine=innodb
comment '用户表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;

-- uuid index
alter table t_user add index idx_uuid(uuid);

-- 初始化数据
insert into t_user(uuid, username) values
('6b083f01fc8211ea9fd01831bfdec6d6', '张三'),
('927727e1fc8211ea9fd01831bfdec6d6', '李四');
-- ################用户表 end


-- ################秒杀商品表
-- 创建表
drop table if exists t_seckill_goods;
create table t_seckill_goods(
	id int unsigned auto_increment primary key,
	good_uuid varchar(32) default '' comment '商品uuid',
	begin_date varchar(20) not null comment '抢购时间',
	seckill_price decimal(18,2) not null comment '秒杀价格',
	amount int default 0 comment '秒杀数量'
) engine=innodb
comment '秒杀商品表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;

alter table t_seckill_goods add index idx_begin_date(begin_date);

-- 初始化数据
insert into t_seckill_goods(good_uuid, begin_date, seckill_price, amount) values
('c7ab5511fc7f11ea9fd01831bfdec6d6', '2020-09-22 16:00:00', 7949, 100),
('a0939771fc8011ea9fd01831bfdec6d6', '2020-09-22 16:00:00', 138, 100),
('a4819a81fc8011ea9fd01831bfdec6d6', '2020-09-22 16:00:00', 359, 100);
-- ################秒杀商品表 end



-- ################用户订单表
-- 创建表
drop table if exists t_orders;
create table t_orders(
	id int unsigned auto_increment primary key,
	good_uuid varchar(32) default '' comment '商品uuid',
	user_uuid varchar(32) default '' comment '用户uuid',
	amount int not null comment '购买数量',
	total_price decimal(18,2) not null comment '总价',
	order_status tinyint(1) default 0 comment '0:未支付,1:已支付',
	create_time varchar(20) default null comment '创建时间',
	invalid_time varchar(20) default null comment '未支付超时时间'
) engine=innodb
comment '用户订单表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;
-- ################用户订单表 end

-- ################用户订单表
-- 创建表
drop table if exists t_orders;
create table t_orders(
	id int unsigned auto_increment primary key,
	good_uuid varchar(32) default '' comment '商品uuid',
	user_uuid varchar(32) default '' comment '用户uuid',
	amount int not null comment '购买数量',
	total_price decimal(18,2) not null comment '总价',
	order_status tinyint(1) default 0 comment '0:未支付,1:已支付',
	create_time varchar(20) default null comment '创建时间'
) engine=innodb
comment '用户订单表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;
-- ################用户订单表 end


-- ################订单支付表
-- 创建表
drop table if exists t_order_pays;
create table t_order_pays(
	id int unsigned auto_increment primary key,
	order_id varchar(32) default '' comment '订单uuid',
	user_uuid varchar(32) default '' comment '用户uuid',
	pay_time varchar(20) not null comment '支付时间'
) engine=innodb
comment '订单支付表'
auto_increment=1
default charset=utf8
collate utf8_general_ci;
-- ################订单支付表 end

-- #####秒杀商品视图
drop view if exists v_sk_goods;
create view v_sk_goods as
select
	g.*,sg.begin_date,sg.seckill_price,sg.amount,tbs.bs_name
from
	t_seckill_goods sg
left join
	t_goods g on g.uuid = sg.good_uuid
left join
	t_business tbs on tbs.uuid = g.bs_uuid
-- #####秒杀商品视图 end
