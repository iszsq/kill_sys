package com.zsq.demo01.entity;

import java.io.Serializable;

/**
 * t_order_pays
 * 订单支付表
 * @author Administrator
 * @date 2020-09-22 13:12:09
 */
public class OrderPays implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * 订单uuid
     */
    private String orderId;

    /**
     * 用户uuid
     */
    private String userUuid;

    /**
     * 支付时间
     */
    private String payTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid == null ? null : userUuid.trim();
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime == null ? null : payTime.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", userUuid=").append(userUuid);
        sb.append(", payTime=").append(payTime);
        sb.append("]");
        return sb.toString();
    }
}