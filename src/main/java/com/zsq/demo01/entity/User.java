package com.zsq.demo01.entity;

import java.io.Serializable;

/**
 * t_user
 * 用户表
 * @author Administrator
 * @date 2020-09-22 13:12:09
 */
public class User implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * uuid
     */
    private String uuid;

    /**
     * 用户名
     */
    private String username;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uuid=").append(uuid);
        sb.append(", username=").append(username);
        sb.append("]");
        return sb.toString();
    }
}