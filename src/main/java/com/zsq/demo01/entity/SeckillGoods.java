package com.zsq.demo01.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * t_seckill_goods
 * 秒杀商品表
 * @author Administrator
 * @date 2020-09-22 13:12:09
 */
public class SeckillGoods implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * 商品uuid
     */
    private String goodUuid;

    /**
     * 抢购时间
     */
    private String beginDate;

    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;

    /**
     * 秒杀数量
     */
    private Integer amount;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoodUuid() {
        return goodUuid;
    }

    public void setGoodUuid(String goodUuid) {
        this.goodUuid = goodUuid == null ? null : goodUuid.trim();
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate == null ? null : beginDate.trim();
    }

    public BigDecimal getSeckillPrice() {
        return seckillPrice;
    }

    public void setSeckillPrice(BigDecimal seckillPrice) {
        this.seckillPrice = seckillPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", goodUuid=").append(goodUuid);
        sb.append(", beginDate=").append(beginDate);
        sb.append(", seckillPrice=").append(seckillPrice);
        sb.append(", amount=").append(amount);
        sb.append("]");
        return sb.toString();
    }
}