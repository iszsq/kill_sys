package com.zsq.demo01.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * v_sk_goods
 * VIEW
 * @author Administrator
 * @date 2020-09-22 15:08:44
 */
public class SkGoods implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * uuid
     */
    private String uuid;

    /**
     * 商家uuid
     */
    private String bsUuid;

    /**
     * 标题
     */
    private String title;

    /**
     * 售价
     */
    private BigDecimal price;

    /**
     * 商品图片
     */
    private String pic;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 抢购时间
     */
    private String beginDate;

    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;

    /**
     * 秒杀数量
     */
    private Integer amount;

    /**
     * 商家名称
     */
    private String bsName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getBsUuid() {
        return bsUuid;
    }

    public void setBsUuid(String bsUuid) {
        this.bsUuid = bsUuid == null ? null : bsUuid.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate == null ? null : beginDate.trim();
    }

    public BigDecimal getSeckillPrice() {
        return seckillPrice;
    }

    public void setSeckillPrice(BigDecimal seckillPrice) {
        this.seckillPrice = seckillPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getBsName() {
        return bsName;
    }

    public void setBsName(String bsName) {
        this.bsName = bsName == null ? null : bsName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uuid=").append(uuid);
        sb.append(", bsUuid=").append(bsUuid);
        sb.append(", title=").append(title);
        sb.append(", price=").append(price);
        sb.append(", pic=").append(pic);
        sb.append(", stock=").append(stock);
        sb.append(", beginDate=").append(beginDate);
        sb.append(", seckillPrice=").append(seckillPrice);
        sb.append(", amount=").append(amount);
        sb.append(", bsName=").append(bsName);
        sb.append("]");
        return sb.toString();
    }
}