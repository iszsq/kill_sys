package com.zsq.demo01.entity;

import java.io.Serializable;

/**
 * t_business
 * 商家表
 * @author Administrator
 * @date 2020-09-22 13:12:09
 */
public class Business implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * uuid
     */
    private String uuid;

    /**
     * 商家名称
     */
    private String bsName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getBsName() {
        return bsName;
    }

    public void setBsName(String bsName) {
        this.bsName = bsName == null ? null : bsName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uuid=").append(uuid);
        sb.append(", bsName=").append(bsName);
        sb.append("]");
        return sb.toString();
    }
}