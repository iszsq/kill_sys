package com.zsq.demo01.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * t_orders
 * 用户订单表
 * @author Administrator
 * @date 2020-09-22 13:12:09
 */
public class Orders implements Serializable {
    /**
     * 
     */
    private Integer id;

    /**
     * 商品uuid
     */
    private String goodUuid;

    /**
     * 用户uuid
     */
    private String userUuid;

    /**
     * 购买数量
     */
    private Integer amount;

    /**
     * 总价
     */
    private BigDecimal totalPrice;

    /**
     * 0:未支付,1:已支付
     */
    private Boolean orderStatus;

    /**
     * 创建时间
     */
    private String createTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoodUuid() {
        return goodUuid;
    }

    public void setGoodUuid(String goodUuid) {
        this.goodUuid = goodUuid == null ? null : goodUuid.trim();
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid == null ? null : userUuid.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Boolean getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime == null ? null : createTime.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", goodUuid=").append(goodUuid);
        sb.append(", userUuid=").append(userUuid);
        sb.append(", amount=").append(amount);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}