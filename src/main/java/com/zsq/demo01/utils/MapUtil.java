package com.zsq.demo01.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public class MapUtil {

    public static Map objectToMap(Object obj){
        Map result= null;

        ObjectMapper mapper = new ObjectMapper(); //转换器
        try {
            String json=mapper.writeValueAsString(obj);
            result = mapper.readValue(json, Map.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

}
