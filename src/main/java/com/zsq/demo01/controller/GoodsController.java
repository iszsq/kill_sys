package com.zsq.demo01.controller;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zsq.demo01.common.PageBody;
import com.zsq.demo01.common.ResultData;
import com.zsq.demo01.dao.GoodsMapper;
import com.zsq.demo01.entity.Goods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * 商品接口
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    private static Logger logger = LoggerFactory.getLogger(GoodsController.class);

    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 获取所有商品列表
     * @return
     */
    @RequestMapping("/list")
    public ResultData getList(@RequestParam(defaultValue = "1")int pageNum, @RequestParam(defaultValue = "10")int pageSize){
        Page<Goods> goodsPage = PageHelper.startPage(pageNum, pageSize);
        List<Goods> goodList = goodsMapper.getGoodList();
        logger.debug("page: {}", goodsPage.toString());
        return ResultData.success(new PageBody(goodsPage));
    }

    /**
     * 获取秒杀商品列表
     * @return
     */
    @RequestMapping("/skList")
    public ResultData getSecKillList(@RequestParam(defaultValue = "1")int pageNum, @RequestParam(defaultValue = "10")int pageSize){
        Page<Goods> goodsPage = PageHelper.startPage(pageNum, pageSize);
        List<Goods> goodList = goodsMapper.getGoodList();
        logger.debug("page: {}", goodsPage.toString());
        return ResultData.success(new PageBody(goodsPage));
    }

}
