package com.zsq.demo01.controller;

import com.zsq.demo01.common.ResultData;
import com.zsq.demo01.dao.GoodsMapper;
import com.zsq.demo01.dao.SeckillGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 秒杀接口
 */
@RestController
@RequestMapping("/secKill")
public class SeckillController {

    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;

    /**
     * 抢购商品
     * @param goodId 商品id
     * @param userId 用户id
     * @param beginDate 秒杀日期
     * @param hour 秒杀时间段
     * @return
     */
    @RequestMapping("/placeOrder")
    @Transactional
    public ResultData placeOrder(@RequestParam int goodId, @RequestParam String userId,
                                 @RequestParam int beginDate, @RequestParam String hour ){
        //从redis中获取，判断商品是否已开始秒杀
        Object o = redisTemplate.opsForHash().get("current_skgoods_" + beginDate + hour, goodId+"");
        if(o == null){
            return ResultData.fail("秒杀未开始");
        }

        //先减库存






        return null;
    }

}
