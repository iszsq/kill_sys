package com.zsq.demo01.dao;

import com.zsq.demo01.entity.OrderPays;
import com.zsq.demo01.entity.OrderPaysExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderPaysMapper {
    long countByExample(OrderPaysExample example);

    int deleteByExample(OrderPaysExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(OrderPays record);

    int insertSelective(OrderPays record);

    List<OrderPays> selectByExample(OrderPaysExample example);

    OrderPays selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") OrderPays record, @Param("example") OrderPaysExample example);

    int updateByExample(@Param("record") OrderPays record, @Param("example") OrderPaysExample example);

    int updateByPrimaryKeySelective(OrderPays record);

    int updateByPrimaryKey(OrderPays record);
}