package com.zsq.demo01.dao;

import com.zsq.demo01.entity.SkGoods;
import com.zsq.demo01.entity.SkGoodsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SkGoodsMapper {
    long countByExample(SkGoodsExample example);

    int deleteByExample(SkGoodsExample example);

    int insert(SkGoods record);

    int insertSelective(SkGoods record);

    List<SkGoods> selectByExample(SkGoodsExample example);

    int updateByExampleSelective(@Param("record") SkGoods record, @Param("example") SkGoodsExample example);

    int updateByExample(@Param("record") SkGoods record, @Param("example") SkGoodsExample example);
}