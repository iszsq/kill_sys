package com.zsq.demo01.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.zsq.**.dao.**")
public class MybatisConfig {



}
