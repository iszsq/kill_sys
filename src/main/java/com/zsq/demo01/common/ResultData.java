package com.zsq.demo01.common;

public class ResultData<T> {

    public enum ResultEnums{
        success(200, "成功"), failed(0, "失败");

        private int code;
        private String name;

        ResultEnums(int code, String name) {
            this.code = code;
            this.name = name;
        }
    }

    private String message;
    private int code;
    private T data;

    public ResultData() {
    }

    public static  ResultData  success( ){
        ResultData resultData = new ResultData();
        resultData.setCode(ResultEnums.success);
        return resultData;
    }

    public static <T> ResultData<T> success(T d){
        ResultData resultData = new ResultData();
        resultData.setCode(ResultEnums.success);
        resultData.setData(d);
        return resultData;
    }

    public static <T> ResultData<T> success(String message, T d){
        ResultData resultData = new ResultData();
        resultData.setCode(ResultEnums.success);
        resultData.setMessage(message);
        resultData.setData(d);
        return resultData;
    }

    public static ResultData fail(String message){
        ResultData resultData = new ResultData();
        resultData.setCode(ResultEnums.failed);
        resultData.setMessage(message);
        return resultData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setCode(ResultEnums codeEnum) {
        this.code = codeEnum.code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResultData{" +
                "message='" + message + '\'' +
                ", code=" + code +
                ", data=" + data +
                '}';
    }
}
