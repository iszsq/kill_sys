package com.zsq.demo01;


import com.github.pagehelper.PageHelper;
import com.zsq.demo01.dao.SeckillGoodsMapper;
import com.zsq.demo01.dao.SkGoodsMapper;
import com.zsq.demo01.entity.*;
import com.zsq.demo01.utils.MapUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Demo01Application.class)
public class TestRedis {

    Logger log = LoggerFactory.getLogger(TestRedis.class);

    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SkGoodsMapper skGoodsMapper;
    @Autowired
    private SeckillGoodsMapper seckillGoodsMapper;

    @Test
    public void testString(){
        String test = stringRedisTemplate.opsForValue().get("test");
        log.debug("redis-test: {}", test);

    }

    @Test
    public void saveMapToRedis(){
        Map<String, String> goods= new HashMap<>();
        goods.put("title","haha");
        goods.put("stock","10");

        stringRedisTemplate.opsForHash().putAll("test_goods_001", goods);
    }

    /**
     * 加载秒杀商品到redis
     */
    @Test
    public void loadSeckillGoods() throws ParseException {
        //获取当前时间段
        String nowTimeStr = "2020-09-22 16:00";

        //取出秒杀商品
        SkGoodsExample example = new SkGoodsExample();
        SkGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andBeginDateEqualTo(nowTimeStr);
        List<SkGoods> skGoods = skGoodsMapper.selectByExample(example);
        log.debug("skGoods: {}", skGoods.toString());

        SeckillGoodsExample sgexample = new SeckillGoodsExample();
        SeckillGoodsExample.Criteria sgCriteria = sgexample.createCriteria();
        sgCriteria.andBeginDateEqualTo(nowTimeStr);
        List<SeckillGoods> seckillGoods = seckillGoodsMapper.selectByExample(sgexample);
        log.debug("seckillGoods: {}", seckillGoods.toString());

        //定义redis的key
        String year = nowTimeStr.substring(0,4);
        String month = nowTimeStr.substring(5,7);
        String day = nowTimeStr.substring(8,10);
        String hour = nowTimeStr.substring(11,13);

        log.debug("year: {}", year);
        log.debug("month: {}", month);
        log.debug("day: {}", day);
        log.debug("hour: {}", hour);

        //当前秒杀商品redis key
        String skgoods_key = "current_skgoods_"+year+month+day+hour;
        Map<String ,String> skGoodsMap = new HashMap<>();
        for(SeckillGoods skg : seckillGoods){
            skGoodsMap.put(skg.getId()+"", skg.getAmount() + "");
        }
        stringRedisTemplate.opsForHash().putAll(skgoods_key, skGoodsMap);
    }
}
