package com.zsq.demo01;

import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SimpleTest {

    @Test
    public void test(){
        Date nowTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTimeStr = dateFormat.format(nowTime);
        System.out.println(nowTimeStr);
    }

    @Test
    public void nextHour(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date nowTime = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTimeStr = dateFormat.format(nowTime);
        System.out.println(nowTimeStr);

        calendar.add(Calendar.HOUR, 1);
        Date nextTime = calendar.getTime();
        String nextTimeStr = dateFormat.format(nextTime);
        System.out.println(nextTimeStr);
    }

}
