package com.zsq.demo01;


import com.zsq.demo01.dao.UserMapper;
import com.zsq.demo01.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Demo01Application.class)
public class TestMybatis {

    Logger log = LoggerFactory.getLogger(TestMybatis.class);

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testMapper(){
        User user = userMapper.selectByPrimaryKey(1);
        assert user != null;
        log.debug(user.toString());
    }
}
